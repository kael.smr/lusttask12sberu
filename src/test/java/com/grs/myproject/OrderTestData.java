package com.grs.myproject;


import com.grs.myproject.cinematronic.dto.FilmDTO;
import com.grs.myproject.cinematronic.dto.OrderDTO;
import com.grs.myproject.cinematronic.dto.UserDTO;
import com.grs.myproject.cinematronic.model.Film;
import com.grs.myproject.cinematronic.model.Order;
import com.grs.myproject.cinematronic.model.User;
import jakarta.persistence.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface OrderTestData {


    OrderDTO FILM_ORDER_DTO = new OrderDTO(
            new UserDTO(),
            new FilmDTO(),
            LocalDate.now(),
            20,
            false,
            1L,
            2L
    );

    List<OrderDTO> FILM_ORDER_DTO_LIST = List.of(FILM_ORDER_DTO);

    Order FILM_ORDER = new Order(
            new User(),
            new Film (),
            LocalDate.now(),
            20,
            false
    );


    List<Order> FILM_ORDER_LIST = List.of(FILM_ORDER);
}
