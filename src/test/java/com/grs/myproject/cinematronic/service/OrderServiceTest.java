package com.grs.myproject.cinematronic.service;

import com.grs.myproject.cinematronic.dto.OrderDTO;
import com.grs.myproject.cinematronic.exception.MyDeleteException;
import com.grs.myproject.cinematronic.model.Order;
import org.junit.jupiter.api.Test;

public class OrderServiceTest extends GenericTest<Order, OrderDTO> {
    @Test
    @Override
    protected void getAll() {
    
    }
    
    @Test
    @Override
    protected void getOne() {
    
    }
    
    @Test
    @Override
    protected void create() {
    
    }
    
    @Test
    @Override
    protected void update() {
    
    }
    
    @Test
    @Override
    protected void delete() throws MyDeleteException {
    
    }
    
    @Override
    protected void restore() {
    
    }
    
    @Test
    @Override
    protected void getAllNotDeleted() {
    
    }
}
