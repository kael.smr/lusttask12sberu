package com.grs.myproject.cinematronic.service;

import com.grs.myproject.cinematronic.dto.UserDTO;
import com.grs.myproject.cinematronic.exception.MyDeleteException;
import com.grs.myproject.cinematronic.model.User;
import org.junit.jupiter.api.Test;

public class UserServiceTest extends GenericTest<User, UserDTO> {
    
    @Test
    @Override
    protected void getAll() {
    
    }
    
    @Test
    @Override
    protected void getOne() {
    
    }
    
    @Test
    @Override
    protected void create() {
    
    }
    
    @Test
    @Override
    protected void update() {
    
    }
    
    @Test
    @Override
    protected void delete() throws MyDeleteException {
    
    }
    
    @Test
    @Override
    protected void restore() {
    
    }
    
    @Test
    @Override
    protected void getAllNotDeleted() {
    
    }
}
