package com.grs.myproject.cinematronic.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class IdFilmDirectorDTO {
    private Long filmId;
    private Long directorId;
}
