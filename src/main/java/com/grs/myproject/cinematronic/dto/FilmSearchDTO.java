package com.grs.myproject.cinematronic.dto;

import com.grs.myproject.cinematronic.model.Genre;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FilmSearchDTO {
    private String filmTitle;
    private String directorsFio;
    private Genre genre;
}
