package com.grs.myproject.cinematronic.constants;

public interface MailConstants {
    String MAIL_MESSAGE_FOR_REMEMBER_PASSWORD = """
            Good afternoon. You have received this email because a password recovery request has been sent from your account.
             To restore your password, follow the link: http://localhost:8080/users/change-password?uuid=""";
    
    String MAIL_SUBJECT_FOR_REMEMBER_PASSWORD = "Password recovery on the Online Movie Library website";
}
