package com.grs.myproject.cinematronic.constants;

public interface UserRoleConstants {
    String ADMIN = "ADMIN";
    String LIBRARIAN = "LIBRARIAN";
    String USER = "USER";
}
