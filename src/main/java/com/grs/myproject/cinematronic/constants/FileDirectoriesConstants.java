package com.grs.myproject.cinematronic.constants;

public interface FileDirectoriesConstants {
    String FILMS_UPLOAD_DIRECTORY = "files/films";
}
