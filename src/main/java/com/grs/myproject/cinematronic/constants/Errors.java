package com.grs.myproject.cinematronic.constants;

public interface Errors {
    class Films {
        public static final String FILM_DELETE_ERROR = "The movie cannot be deleted because it has active leases";
    }
    
    class Directors {
        public static final String DIRECTOR_DELETE_ERROR = "The director cannot be removed, since his films have" +
                " there are active leases";
    }
    
    class Users {
        public static final String USER_FORBIDDEN_ERROR = "You do not have rights to view user information";
    }
}
