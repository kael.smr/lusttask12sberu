package com.grs.myproject.cinematronic.mapper;

import com.grs.myproject.cinematronic.dto.GenericDTO;
import com.grs.myproject.cinematronic.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {
    E toEntity(D dto);
    
    D toDTO(E entity);
    
    List<E> toEntities(List<D> dtoList);
    
    List<D> toDTOs(List<E> entitiesList);
}
