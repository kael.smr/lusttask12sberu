package com.grs.myproject.cinematronic.rest;

import com.grs.myproject.cinematronic.dto.FilmDTO;
import com.grs.myproject.cinematronic.dto.OrderDTO;
import com.grs.myproject.cinematronic.dto.UserDTO;
import com.grs.myproject.cinematronic.model.Order;
import com.grs.myproject.cinematronic.service.FilmService;
import com.grs.myproject.cinematronic.service.OrderService;
import com.grs.myproject.cinematronic.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/orders")
@Tag(name = "Orders", description = "Controller for working with orders")
public class OrderController extends GenericController<Order, OrderDTO> {

    private final OrderService orderService;
    private final FilmService filmService;
    private final UserService userService;

    public OrderController(OrderService orderService, FilmService filmService, UserService userService) {
        super(orderService);
        this.orderService = orderService;
        this.filmService = filmService;
        this.userService = userService;
    }

    @Operation(description = "Rent a movie", method = "create")
    @RequestMapping(value = "/addOrder", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDTO> create(@RequestBody OrderDTO newEntity) {
        FilmDTO filmDTO = filmService.getOne(newEntity.getFilmId());
        UserDTO userDTO = userService.getOne(newEntity.getUserId());
        filmDTO.getOrdersIds().add(newEntity.getId());
        userDTO.getOrdersIds().add(newEntity.getId());

        return ResponseEntity.status(HttpStatus.CREATED).body(orderService.create(newEntity));
    }
}

