package com.grs.myproject.cinematronic.rest;

import com.grs.myproject.cinematronic.dto.DirectorDTO;
import com.grs.myproject.cinematronic.dto.FilmDTO;
import com.grs.myproject.cinematronic.model.Director;
import com.grs.myproject.cinematronic.service.DirectorService;
import com.grs.myproject.cinematronic.service.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/directors")
@Tag(name = "Directors", description = "Controller for working with film directors")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DirectorController extends GenericController<Director, DirectorDTO> {

    private final DirectorService directorService;
    private final FilmService filmService;

    public DirectorController(DirectorService directorService, FilmService filmService) {
        super(directorService);
        this.directorService = directorService;
        this.filmService = filmService;
    }

    @Operation(description = "Add a movie to the director", method = "addFilm")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDTO> addFilm(@RequestParam(value = "filmId") Long filmId,
                                               @RequestParam(value = "directorId") Long directorId) {
        DirectorDTO directorDTO = directorService.getOne(directorId);
        FilmDTO filmDTO = filmService.getOne(filmId);
        directorDTO.getFilmsIds().add(filmDTO.getId());
        return ResponseEntity.status(HttpStatus.OK).body(directorService.update(directorDTO));
    }

}
